#!/usr/bin/python3

import argparse

import json2rating

def cmdline():
    parser = argparse.ArgumentParser(
        description='Rate Skytear players from matchreportsDB.json database dump')
    parser.add_argument('dbfilename')
    json2rating.process_common_args(parser)

    parser.add_argument('--all', action='store_true',
                        help="dump all periods instead of last complete one")
    parser.add_argument('--current', action='store_true',
                        help="dump current (unfinished) period instead of last complete one")
    parser.add_argument('--player', type=str,
                        help="dump only data for specified player")
    parser.add_argument('--opponents', action='store_true',
                        help="include opponents and results in dump")

    return parser.parse_args()

def dump_ratings(periods, args):
    for period_num, period in enumerate(periods):
        if ((period_num == len(periods) - 2 and not args.current) or
            (period_num == len(periods) - 1 and args.current) or
            args.all):

            print(f"Period {1+period_num}: {period.name} - {period.rating_time.strftime('%c %Z')}, {len(period.opponents)} players, {len(period.games)} games")
            num_players = len(period.ratedplayers)
            sorted_players = sorted(period.ratedplayers, key=lambda p: (period.final_ratings[p][0],
                                                                        -period.final_ratings[p][0]))
            lowest_rating = period.final_ratings[sorted_players[0]][0]
            highest_rating = period.final_ratings[sorted_players[-1]][0]
            for invrank, player in enumerate(sorted_players):
                # skip unrequested players
                if args.player != None and args.player not in player.names:
                    continue

                player_rating, player_rd = period.final_ratings[player]
                player_position = 100 * (player_rating - lowest_rating) / (highest_rating - lowest_rating)
                print(f"\t{player_rating:7.02f} / {player_rd:6.02f}"
                      f"  #{num_players-invrank:3d} {player_position:3.0f}% {player.display_name}", end='')
                if player in period.initial_ratings:
                    print(f" ({player_rating - period.initial_ratings[player][0]:+.02f} / {player_rd - period.initial_ratings[player][1]:+.02f})", end='')
                    if args.opponents:
                        for opponent, result in zip(period.opponents[player],
                                                    period.game_results[player]):
                            print("  %s %s" % ({0:"-", 1:"+", 0.5:"="}[result],
                                               opponent.display_name), end='')
                print()

####

if __name__ == "__main__":
    args = cmdline()
    games = json2rating.get_data(args.dbfilename)
    if args.fix:
        json2rating.fix_data(games)
    num_raw_records = len(games)
    games = json2rating.filtered_data(games)
    num_filtered_records = len(games)

    print(f"rating games: {num_filtered_records} / {num_raw_records}")

    if args.season:
        periods = json2rating.event_based_periods(games, events=json2rating.COMPETITIVE[args.season])
        print(f"{len(periods)} periods of season {args.season}")
    else:
        periods = json2rating.time_periods(games, ndays=args.days)
        print(f"{len(periods)} {args.days}-days periods")

    #

    players = {}
    json2rating.compute_ratings(players, periods)
    dump_ratings(periods, args)
