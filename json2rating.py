from collections import OrderedDict
from datetime import datetime, timezone, timedelta
import dateutil.parser
import json

from glicko2.glicko2 import glicko2

# "2vs2 Team League" to filter out
TOURNAMENT_2V2LEAGUE = "96AW9wq6GKvnK0B2RHmz"

COMPETITIVE = {"2020": (("Pk8HGHSqywAl8pXtMA5Q", "July WQ"),
                        ("3OfxySU5EO06rHLg1GHX", "August WQ"),
                        ("RfIKmyZth6rgsmmipyiy", "Modena WQ"),
                        ("eEvWS4JjWEFz20Esiypb", "September WQ"),
                        ("bHfwvP23DIjMvlMrSXFm", "Game Firenze WC"),
                        ("oLxJJEkdPMeYfCYsKPFj", "Dungeonstore Genova WC"),
                        ("NUU55mRQhH0f7vLSuulH", "October WQ"),
                        ("CVdvgHpgoKEcmp1uYDnd", "American championship"),
                        ("PyxmX8Z48mDpDrpyGriG", "European championship"),
                        ("SX98ufXM86NWxtvDTZKX", "November WQ"),
                        ("RNgolhGFZlmPhMS4eTGX", "Last Chance WQ"),
                        ("49SVwUO5coCBDbEVc7pb", "Worlds"),
                        ),
               "2021": (
                   ("f4deRGWlqg16eLFjqEmO", "February WQ"),
                   ("MF6uoUBSuobiopEbqRJQ", "March WQ"),
                   ("gXGL60scLFuPGdwuA6zG", "April WQ"),
                   ("6msI8hELvD36R8R4Jcx4", "May WQ"),
                   ),
               }

START_DATES = {
    "casual": datetime(2020, 4, 1, tzinfo=timezone.utc),
    "2020": datetime(2020, 7, 1, tzinfo=timezone.utc),
    "2021": datetime(2021, 1, 1, tzinfo=timezone.utc),
}

def process_common_args(parser):
    parser.add_argument('--days', type=int, default=1,
                        help="number of days in each rating period")
    parser.add_argument('--season', type=str,
                        help="rate only competitive games for specified season")
    parser.add_argument('--fix', action='store_true',
                        help="fix known errors in the records")

def sortkey(game):
    time = game['data']['GameTime']
    return (time['seconds'], time['nanoseconds'])

def get_data(filename):
    with open(filename) as db:
        return json.load(db)

def game_from_id(games, gameid):
    matches = [ g for g in games if g['id'] == gameid ]
    assert len(matches) == 1, f"several games with id {gameid}: {matches}"
    return matches[0]

def fix_data(games):
    # Yann vs Uptrax reversed
    game_from_id(games, "fBoIrFvPECE2zx5LN5MJ")['data']['Winner'] = 'First player team won'
    # Kalibra name previously fixed but not Uid
    game_from_id(games, "jA7SX8RABYTg2DFuYzy2")['data']['TeamOnePlayerUid'] = "yUpYerIW43bs5HbzPBAfn21H2hD2"
    # previously unreported Nov WC game
    g = game_from_id(games, "yzkwmMTe15NFNi4JKOh1")
    g['data']['tournamentID'] = "SX98ufXM86NWxtvDTZKX"
    d = dateutil.parser.parse("November 28, 2020")
    g['data']['GameTime']['seconds'] = int(d.timestamp())

def filtered_data(data):
    return sorted( (game for game in data if ('TeamOnePlayerUid' in game['data']
                                              and game['data']['TeamOnePlayerUid'] != "0"
                                              and 'TeamTwoPlayerUid' in game['data']
                                              and game['data']['TeamTwoPlayerUid'] != "0"
                                              and 'tournamentID' in game['data']
                                              and game['data']['tournamentID'] != 0
                                              and game['data']['tournamentID'] != TOURNAMENT_2V2LEAGUE
                                              )
                    ), key=sortkey )

class Period(object):
    def __init__(self, name, rating_time):
        self.name = name
        self.rating_time = rating_time
        #
        self.games = []
        self.ratedplayers = None # set of players seen in all periods up to now
        # for each player:
        self.initial_ratings = {} # ratings and RDs
        self.opponents = {}
        self.game_results = {}
        self.final_ratings = {} # ratings and RDs
    def num_games(self):
        return len(self.games)

def time_periods(games, ndays):
    "regroup games in `ndays`-long rating periods"
    PERIOD_LENGTH = 3600 * 24 * ndays
    RATING_START = games[0]['data']['GameTime']['seconds']
    RATING_END = games[-1]['data']['GameTime']['seconds']
    num_periods = 1 + (RATING_END - RATING_START) // PERIOD_LENGTH

    periods = [ Period((start := datetime.fromtimestamp(RATING_START + i * PERIOD_LENGTH,
                                                        timezone.utc)).strftime("%c"),
                       start + timedelta(seconds=PERIOD_LENGTH))
                for i in range(num_periods) ]

    for game in games:
        period = (game['data']['GameTime']['seconds'] - RATING_START) // PERIOD_LENGTH
        periods[period].games.append(game)

    return periods

def event_based_periods(games, events):
    events_id_list = [ tid for tid, tname in events ]
    periods = [ Period(name=tname, rating_time=None) for tid, tname in events ]
    for game in games:
        tid = game['data']['tournamentID']
        if tid in events_id_list:
            periods[events_id_list.index(tid)].games.append(game)

    for period in periods:
        if len(period.games):
            period.rating_time = datetime.fromtimestamp(
                period.games[-1]['data']['GameTime']['seconds'],
                timezone.utc)

    return periods

###

class Player(glicko2.Player):
    def __init__(self, id):
        super().__init__()
        self.id = id
        self.names = set()
    @property
    def display_name(self):
        return " aka. ".join(self.names)

def game_scores(game):
    return {
        'First player team won': (1, 0),
        'Second player team won': (0, 1),
        'It was a tie': (0.5, 0.5),
    }[game['data']['Winner']]

def compute_ratings(players, periods):
    ratedplayers = set() # accumulate period after period
    for period in periods:
        # collect players, initial ratings, game result data
        opponent_ratings = {}
        opponent_rds = {}
        for game in period.games:
            id1 = game['data']['TeamOnePlayerUid']
            id2 = game['data']['TeamTwoPlayerUid']
            # create players if needed
            if id1 not in players:
                players[id1] = Player(id1)
            if id2 not in players:
                players[id2] = Player(id2)
            p1, p2 = players[id1], players[id2]
            ratedplayers.add(p1)
            ratedplayers.add(p2)
            # keep all display aliases for each player
            p1.names.add(game['data']['TeamOneDisplayName'])
            p2.names.add(game['data']['TeamTwoDisplayName'])
            #
            if p1 not in period.initial_ratings:
                period.initial_ratings[p1] = (p1.rating, p1.rd)
                period.opponents[p1] = []
                opponent_ratings[p1] = []
                opponent_rds[p1] = []
                period.game_results[p1] = []
            if p2 not in period.initial_ratings:
                period.initial_ratings[p2] = (p2.rating, p2.rd)
                period.opponents[p2] = []
                opponent_ratings[p2] = []
                opponent_rds[p2] = []
                period.game_results[p2] = []
            #
            opponent_ratings[p1].append(period.initial_ratings[p2][0])
            opponent_rds[p1].append(period.initial_ratings[p2][1])
            opponent_ratings[p2].append(period.initial_ratings[p1][0])
            opponent_rds[p2].append(period.initial_ratings[p1][1])
            #
            period.opponents[p1].append(p2)
            period.opponents[p2].append(p1)
            p1_score, p2_score = game_scores(game)
            period.game_results[p1].append(p1_score)
            period.game_results[p2].append(p2_score)

        period.ratedplayers = set(ratedplayers)

        # compute new ratings
        for player in ratedplayers:
            if player not in period.initial_ratings:
                player.did_not_compete()
            else:
                player.update_player(opponent_ratings[player], opponent_rds[player],
                                     period.game_results[player])
            period.final_ratings[player] = (player.rating, player.rd)

def player_uids(players, player_name):
    return [uid for uid, player in players.items()
            if player_name in player.names]

def player_uid(players, player_name):
    uids = player_uids(players, player_name)
    assert len(uids) == 1, "name %r does not matches a single uid: %s" % (player_name, uids)
    return uids[0]
