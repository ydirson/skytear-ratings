#!/usr/bin/python3

import argparse
import os

import pandas
import altair

import json2rating

def cmdline():
    parser = argparse.ArgumentParser(
        description='Rate Skytear players from matchreportsDB.json database dump')
    parser.add_argument('dbfilename')
    json2rating.process_common_args(parser)

    parser.add_argument('--player', type=str,
                        help="dump only date for specified player")
    parser.add_argument('--outdir', type=str, required=True,
                        help="output directory")

    return parser.parse_args()

def graph_ratings(title, periods, player, outdir):
    dates = [json2rating.START_DATES[title]] + [
        period.rating_time
        for period in periods if player in period.final_ratings ]
    ratings = [1500] + [
        period.final_ratings[player][0]
        for period in periods if player in period.final_ratings ]
    rds = [350] + [
        period.final_ratings[player][1]
        for period in periods if player in period.final_ratings ]
    labels = ['initial rank'] + [
        f'''{period.name}
{len(period.game_results[player] if player in period.game_results else [])} games
{period.final_ratings[player][0]:.02f}'''
        for period in periods if player in period.final_ratings ]
    source = pandas.DataFrame({'date': dates, 'rating': ratings, 'rd': rds,
                               'label': labels})

    base_chart = altair.Chart(source).transform_calculate(
        min='datum.rating - 2 * datum.rd', max='datum.rating + 2 * datum.rd'
    ).properties(
        title=dict(text=f"{title} ratings for {player.display_name}",
                   # subtitle=f"{player.id}",
                   )
    )

    # ranking chart and error band
    chart = base_chart.mark_line(interpolate='step-after').encode(
        x='date', y=altair.Y('rating',
                             title='rating',
                             scale=altair.Scale(domain=(500,2500))))
    band = base_chart.mark_area(opacity=0.3).encode(
        x='date', y=altair.Y('min:Q'), y2=altair.Y2('max:Q')
    )

    # mouse selection (following multiline_tooltip altair example)
    nearest = altair.selection(type='single', nearest=True, on='mouseover',
                               fields=['date'], empty='none')
    selectors = altair.Chart(source).mark_point().encode(
        x='date',
        opacity=altair.value(0),
    ).add_selection(
        nearest
    )
    points = chart.mark_point().encode(
        opacity=altair.condition(nearest, altair.value(1), altair.value(0))
    )
    text = chart.mark_text(align='left', lineBreak='\n', dx=5, dy=-5).encode(
        text=altair.condition(nearest, 'label', altair.value(' '))
    )
    rules = altair.Chart(source).mark_rule(color='gray').encode(
        x='date',
    ).transform_filter(
        nearest
    )

    altair.layer(
        chart, band, selectors, points, rules, text
    ).save(os.path.join(outdir, f"{player.id}.html"))

####

if __name__ == "__main__":
    args = cmdline()
    games = json2rating.get_data(args.dbfilename)
    if args.fix:
        json2rating.fix_data(games)
    games = json2rating.filtered_data(games)

    if args.season:
        periods = json2rating.event_based_periods(games, events=json2rating.COMPETITIVE[args.season])
        title = args.season
    else:
        periods = json2rating.time_periods(games, ndays=args.days)
        title = "casual"

    os.makedirs(args.outdir, exist_ok=True)

    #

    players = {}
    json2rating.compute_ratings(players, periods)
    if args.player:
        graph_ratings(title, periods,
                      players[json2rating.player_uid(players, args.player)],
                      args.outdir)
    else:
        # show most recent ratings in table
        period = periods[-1]
        num_players = len(period.ratedplayers)
        sorted_players = sorted(period.ratedplayers,
                                key=lambda p: (period.final_ratings[p][0],
                                               -period.final_ratings[p][0]))
        lowest_rating = period.final_ratings[sorted_players[0]][0]
        highest_rating = period.final_ratings[sorted_players[-1]][0]

        html_header = f"""
<html>
 <head>
  <title>Skytear {title} ratings</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style>
   table {{
       border: solid black;
       border-collapse: collapse;
   }}
   td {{
       //border-bottom: thin solid #333;
       padding: 1px 10px;
   }}
   thead {{
       background-color: #333;
       color: #fff;
       text-align: center;
   }}
   tbody tr:nth-child(odd) {{
     background: #eee;
   }}
  </style>
 </head>
 <body>
  <h1>Skytear {title} ratings</title></h1>
  <table>
   <thead>
    <td>rank</th>
    <th>rating / dev.</th>
    <th>where%</th>
    <th>player name(s)</th>
   </thead>
   <tbody>
"""
        html_player_tmpl = """
    <tr>
     <td>#{rank}</td>
     <td>{player.rating:.02f} / {player.rd:.02f}</td>
     <td>{player_position:.0f}%</td>
     <td><a href="{player.id}.html">{player.display_name}</a></td>
    </tr>
"""
        html_footer = """
   </tbody>
  </table>
 </body>
</html>
"""
        with open(os.path.join(args.outdir, "index.html"), "xt") as out:
            print(html_header, file=out)

            for rank, player in enumerate(reversed(sorted_players)):
                # skip unrequested players
                if args.player != None and args.player not in player.names:
                    continue

                # player entry in rank table
                player_position = 100 * (player.rating - lowest_rating) / (highest_rating - lowest_rating)
                print(html_player_tmpl.format(player=player,
                                              player_position=player_position,
                                              rank=1+rank),
                      file=out)

                # player's page with graph
                graph_ratings(title, periods, player,
                              args.outdir)

            print(html_footer, file=out)
